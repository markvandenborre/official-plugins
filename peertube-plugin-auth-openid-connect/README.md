# OpenID Connect auth plugin for PeerTube

Add OpenID Connect support to login form in PeerTube.

The initial code of this plugin has been developed with the financial support of the "Direction du Numérique pour l'Éducation du Ministère de
l'Éducation et de la Jeunesse" (french Ministry of National Education).
